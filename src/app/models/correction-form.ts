import { Correction } from './correction';

import * as moment from 'moment';

export class CorrectionForm {
    date: Date;
    weekdays: Date[];
    projects: string[];
    corrections: Correction[];
}
