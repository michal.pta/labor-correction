import { TestBed, inject } from '@angular/core/testing';

import { CorrectionService } from './correction.service';

describe('CorrectionService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [CorrectionService]
    });
  });

  it('should be created', inject([CorrectionService], (service: CorrectionService) => {
    expect(service).toBeTruthy();
  }));
});
