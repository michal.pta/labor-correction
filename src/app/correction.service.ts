import { Injectable } from '@angular/core';

import * as moment from 'moment';
import { Correction } from './models/correction';
import { Observable } from 'rxjs/Observable';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/do';
import 'rxjs/add/operator/share';

import { CorrectionForm } from './models/correction-form';

@Injectable()
export class CorrectionService {

  private correctionForm: CorrectionForm;

  constructor() {
    const form = this.getOrCreateForm();
    this.correctionForm = form;
    console.log('INIT', this.correctionForm);
  }

  getCorrectionForm = () => this.correctionForm;

  getCorrection(project: string, date: Date): Correction {
    const correction = this.correctionForm.corrections
      .find(c => c.project === project && moment(c.date).date() === moment(date).date());
    return correction ? correction : new Correction(project, date, null);
  }

  updateCorrection(correction: Correction) {
    if (!this.correctionForm.corrections.includes(correction)) {
      this.correctionForm.corrections.push(correction);
    } else {
      if (!correction.value) {
        this.correctionForm.corrections = this.correctionForm.corrections.filter(c => c !== correction);
      }
    }
    console.log('UPDATE', this.correctionForm);
    localStorage.setItem('labor-correction-form', JSON.stringify(this.correctionForm));
  }

  getOrCreateForm() {

    let form = JSON.parse(localStorage.getItem('labor-correction-form'));
    if (form) {
      return form;
    }

    form = new CorrectionForm();
    form.date = moment().toDate();
    form.weekdays = this.getWeekdays(form.date);
    form.projects = [
      'project1', 'project2', 'project3'
    ];
    form.corrections = [
      { date: moment().subtract(2, 'days').toDate(), project: 'project1', value: 3, id: 1 },
      { date: moment().subtract(5, 'days').toDate(), project: 'project2', value: -4, id: 1 },
      { date: moment().subtract(1, 'days').toDate(), project: 'project3', value: 2, id: 1 },
    ];
    return form;

  }

  getWeekdays(weekEndingDate: Date) {
    const weekdays: Date[] = [];
    for (let index = 0; index < 7; index++) {
        weekdays.push(moment(weekEndingDate).subtract(6 - index, 'days').toDate());
    }
    return weekdays;
  }

}
