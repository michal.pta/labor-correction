import { Component, OnInit } from '@angular/core';
import * as moment from 'moment';
import { CorrectionService } from '../../correction.service';
import { CorrectionForm } from '../../models/correction-form';

@Component({
  selector: 'app-correction-form',
  templateUrl: './correction-form.component.html',
  styleUrls: ['./correction-form.component.css']
})
export class CorrectionFormComponent implements OnInit {

    correctionForm: CorrectionForm;

    constructor(private correctionService: CorrectionService) {}

    ngOnInit() {
      this.correctionForm = this.correctionService.getCorrectionForm();
    }

  }
